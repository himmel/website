# c3heaven.de

This repository contains the contents of <https://c3heaven.de/>.

The directory `public` contains all contents that will be deployed directly
to the website.

## Deployment via CI

The website can be deployed to our web server using Gitlab CI. The
target server, user and path as well as the SSH key used for deployment are
configured in the settings of this project and the server can be prepared
using the `website-deployment` role from our
[infrastructure repo](https://chaos.expert/himmel/infrastruktur).

`PUBLISH_PATH` is always `/`. `PUBLISH_HOST`, `PUBLISH_USER` and
`SSH_PRIVATE_KEY` must match the values configured via Ansible.

The deployment job is a manual action. After the deployment, the website will
be reachable at <https://c3heaven.de/>.

**Please note that the deployment job will overwrite additional content that
has been added to the deployment target location manually!**
